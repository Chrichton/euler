(defn digits [n]
  (map #(Integer/parseInt (str %))  (seq (str n))))

(defn chapernows []
	(flatten (map digits (iterate inc 1))))

(defn chapernow-digit [n]
	(first (drop (dec n) (chapernows))))

(chapernow-digit 12)

(* (chapernow-digit 1) (chapernow-digit 10) (chapernow-digit 100) (chapernow-digit 1000) 
	(chapernow-digit 10000) (chapernow-digit 100000) (chapernow-digit 1000000))