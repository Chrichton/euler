(def input '(
    (131 673 234 103  18)
	(201  96 342 965 150)
	(630 803 746 422 111)
	(537 699 497 121 956)
	(805 732 524  37 331)))

(defn local-min [x y]
	(cond
		(nil? x) y
		(nil? y) x
		:else (min x y)))

(defn next-row [row1 row2]
	(loop [[x1 & r1] row1 [x2 & r2] row2 accu []]
		(if (nil? x1) accu (recur r1 r2 (conj accu (+ x2 (local-min x1 (last accu))))))))

(next-row (reductions + (first input)) (second input)) ; [332 428 770 1735 1309]

(defn min-path [mat]
	(loop [min-row-accu (reductions + (first mat)) m (rest mat) ]
		(if (empty? m) (last min-row-accu)
			(recur (next-row min-row-accu (first m)) (rest m)))))

(min-path input)

(def input2 (slurp "matrix.txt"))

(def matrix (map (fn [sl] (map read-string sl)) (map #(clojure.string/split % #",") (clojure.string/split-lines input2))))

(count matrix)
(count (first matrix))
(count (last matrix))

(min-path matrix)