(def ru (apply + (map #(* % %)(range 3 1002 2))))

(def rd (apply + (reductions + (cons 3 (map #(+ (* 2 %) (* 2 (inc %))) (range 2 1000 2))))))

(def ld (apply + (reductions + (cons 5 (map #(+ % (* 2 (inc %)) (inc (inc %))) (range 2 1000 2))))))

(def lu (apply + (reductions + (cons 7 (map #(+ (* 2 (inc %)) (* 2 (inc (inc %)))) (range 2 1000 2))))))

(+ 1 ru rd ld lu)