(defn factorial [n]
	(loop [n1 n accu 1]
		(if (zero? n1) accu
			(recur (dec n1) (* accu n1)))))

(factorial (bigint 100))