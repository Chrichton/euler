(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(apply + (distinct (map #(second %) (filter #(and (= 9 (count (first %))) (= 9 (count (distinct (first %)))) (not (zero? (apply * (first %)))))
	(for [multiplicant (range 2 100) multiplier (range 100 10000)]
		(list (concat (digits multiplicant) (digits multiplier) (digits (* multiplicant multiplier))) (* multiplicant multiplier)))))))
