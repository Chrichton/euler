
; Lattice-Path (Wolfram Alpha) (binomial-coefficient (+ a b) a)

(defn factorial [n]
   {:pre [(>= n 0)] }
  	(apply * (range 1N (inc n))))

(defn binomial-coefficient [n k]
   {:pre [(and (>= n 0) (>= n k))]}
   (/ (factorial n) (* (factorial k) (factorial (- n k)))))

(binomial-coefficient 40 20)
