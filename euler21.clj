(defn proper-divisors [number]
		(loop [i 1 accu '()]
			(cond
				(>= i number) accu
				(zero? (mod number i)) (recur (inc i) (cons i accu))
				:else (recur (inc i) accu))))

(defn sum-of-divisors [n]
	(apply + (proper-divisors n)))

(sum-of-divisors 220)
(sum-of-divisors 284)

(defn amicable? [n]
	(let [a (sum-of-divisors n) b (sum-of-divisors a)]
		(and (not= a b) (= b n))))

(amicable? 0)
(amicable? 1)
(amicable? 2)
(amicable? 220)
(amicable? 284)
(amicable? 285)

(apply + (filter amicable? (range 1 10000)))

