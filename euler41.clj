(defn prime? [n]
   (if (< n 2) false
      (loop [x n acc 2]
          (cond 
            (= x acc) true
            (zero? (rem x acc)) false
            :else (recur x (inc acc))))))

(defn all-but-nth [coll n]
    "Returns a list of all items in coll except the nth item. (zero indexed)"
    (lazy-cat (take n coll) (drop (inc n) coll)))

(defn permutations [coll]
    "Returns a lazy list of all permutations of coll."
    (if (= 1 (count coll))
        [coll]
        (for [i (range (count coll)) p (permutations (all-but-nth coll i))]
            (lazy-cat [(nth coll i)] p))))

(defn list-to-number [l]
    (reduce + (map * l (reverse (take (count l) (iterate #(* 10 %) 1))))))

(list-to-number '(1 2 3 4 5 6 7 8 9))

(take 6 (permutations '(9 8 7 6 5 4 3 2 1)))

(first (drop-while #(not (prime? %)) (map list-to-number (permutations '(0 1 1)))))

(defn max-prime [l]
    (first (drop-while #(not (prime? %)) (map list-to-number (permutations l)))))

(first (drop-while nil? (map #(max-prime %) (iterate #(rest %) '(9 8 7 6 5 4 3 2 1)))))
