(defn gcd "eucledian algorithm" [a b]
	(if (zero? b) a
		(gcd b (mod a b))))

(defn resilient? [d]
	(fn [n] (= 1 (gcd n d))))


(defn phi [n]
	(count (filter (resilient? n) (range 1 n))))

(phi 2)  ;1 	2
(phi 3)  ;2 	1.5
(phi 4)  ;2 	2
(phi 5)  ;4 	1.25
(phi 6)  ;2 	3
(phi 7)  ;6  	1.16
(phi 8)  ;4 	2
(phi 9)  ;6 	1.5
(phi 10) ;4 	2.5
(phi 11) ;10 	1.1

(defn n-phi [n]
	(double (/ n (phi n))))

(defn max-pair [a b]
	(if (>= (first a) (first b)) a b))

(defn max-phi [n]
	(reduce max-pair (map #(list (n-phi %) %) (range 2 n))))

(max-phi 10)

;(max-phi 10000)

(map n-phi (range 2 12))

(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

;(last (take-while #(< % 1E6) (lazy-primes))) ; 999983 ()

;(def primes (reverse (drop 20 (take-while #(< % 1E6) (lazy-primes)))))

(defn max-phi [from to]
	(reduce max-pair (map #(list (n-phi %) %) (range from (inc to))))

;(reduce max-pair (map #(list (n-phi (dec %)) ( dec %)) (take 40 primes)))

;(* 2 3 5 7 11 13 17)

(n-phi 510510)
