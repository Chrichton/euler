(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn equal-digits? [n1 n2 n3]
	(and (= (- n3 n2) (- n2 n1)) (= (sort (digits n1)) (sort (digits n2)) (sort (digits n3)))))

(equal-digits? 1487 4817 8147)
(equal-digits? 1485 5817 8157)
(equal-digits? 1234 1235 1236)

(def primes (take-while #(< % 10000) (drop-while #(< % 1000) (lazy-primes))))

(for [f primes s primes t primes 
	:when (and (< f s t) (equal-digits? f s t))]
		(list f s t))
