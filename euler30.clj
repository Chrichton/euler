(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn sum-of-power? [exp]
	(fn [n]
		(= n (apply + (map #(int (Math/pow % exp)) (digits n))))))

((sum-of-power? 4) 1634)
((sum-of-power? 4) 1635)

(filter (sum-of-power? 4)(range 1000 10000))

(apply + (filter (sum-of-power? 5)(range 1000 10000000)))