(def word-map 
	{1 "one"
	 2 "two"
	 3 "three"
	 4 "four"
	 5 "five"
	 6 "six"
	 7 "seven"
	 8 "eight"
	 9 "nine"
	 10 "ten"
	 11 "eleven"
	 12 "twelve"
	 13 "thirteen"
	 14 "fourteen"
	 15 "fifteen"
	 16 "sixteen"
	 17 "seventeen"
	 18 "eighteen"
	 19 "nineteen"
	 20 "twenty"
	 30	"thirty"
	 40 "forty"
	 50 "fifty"
	 60 "sixty"
	 70 "seventy"
	 80 "eighty"
	 90 "ninety"
	 100 "onehundred"
	 200 "twohundred"
	 300 "threehundred"
	 400 "fourhundred"
	 500 "fivehundred"
	 600 "sixhundred"
	 700 "sevenhundred"
	 800 "eighthundred"
	 900 "ninehundred"
	 1000 "onethousand"})

(defn number-to-word [n]
	{:pre [(and (> n 0) (<= n 1000))]}
	(loop [x n accu ""]
		(cond 
			(not (nil? (word-map x))) (concat (word-map x) accu)
			(not (zero? (int (/ x 100)))) 
				(let [hundreds (int (/ x 100))]
					(recur (- x (* hundreds 100)) (concat (word-map hundreds) (concat "hundredand" accu))))
			(not (zero? (int (/ x 10)))) 
				(let [tens (* 10 (int (/ x 10)))] 
					(recur (- x tens) (concat (word-map tens) accu))))))

(count (number-to-word 342))
(count (number-to-word 115))
(number-to-word 24)
(number-to-word 231)
(number-to-word 640)

(map number-to-word (range 1 1001))

(apply + (map count (map number-to-word (range 1 1001))))
