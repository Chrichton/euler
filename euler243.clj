#_(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

#_(defn resilient? [d]
	(fn [n]
		(every? #(not (and (zero? (mod n %)) (zero? (mod d %)))) (take-while #(<= % n) (lazy-primes)))))



(defn gcd "eucledian algorithm" [a b]
	(if (zero? b) a
		(gcd b (mod a b))))

(defn resilient? [d]
	(fn [n] (= 1 (gcd n d))))

((resilient? 12) 1)
((resilient? 12) 2)
((resilient? 12) 3)
((resilient? 12) 4)
((resilient? 12) 5)
((resilient? 12) 6)
((resilient? 12) 7)
((resilient? 12) 8)
((resilient? 12) 9)
((resilient? 12) 10)
((resilient? 12) 11)

(defn R [d] 
	(/ (count (filter (resilient? d) (range 1 d))) (dec d)))

(R  12) ; 4/11
(R 111) ; 36/55

;(first (drop-while #(>= (R %) (/ 2 6)) (iterate inc 12))) ; 30

;(first (drop-while #(>= (R %) (/ 15499 94744)) (iterate inc 12)))

(* 5 7 2707)

(defn Ro [prime-factors]
	(let [product (apply * prime-factors)]
		(loop [[f & r ] prime-factors accu 0]
			(if (nil? f) (/ (- (dec product) accu) (dec product))
				(recur r (+ accu (dec (/ product f))))))))

