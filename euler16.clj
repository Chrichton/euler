(defn digits [n]
  (map #(Integer/parseInt (str %))  (seq (str n))))

(defn mult [n]
	(loop [n1 n acc 1]
		(if (zero? n1) acc
			(recur (dec n1) (* acc (bigint 2))))))

(apply + (digits (mult 1000)))