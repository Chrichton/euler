(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn power [b x]
	(reduce * (repeat x b)))

(defn duplicates? [l]
	(not= (count l) (count (distinct l))))

(defn list-to-number [l]
    (reduce + (map * l (reverse (take (count l) (iterate #(* 10 %) 1))))))

(duplicates? '(1 2 3))
(duplicates? '(1 2 1))

(defn pandingial-multiple? [n]
	(loop [factor 1 accu (digits n)]
		(cond
			(or (duplicates? accu) (> (count accu) 9)) nil
			(and (> factor 1) (= 9 (count accu)) (not= 0 (apply * accu))) accu
			:else (recur (inc factor) (concat accu (digits (* (inc factor) n)))))))
		
(defn range-9 [exponent]
	(range (dec (int (power 10 (inc exponent)))) (dec (* 9 (power 10 exponent))) -1))

(range-9 1)

(pandingial-multiple? 8)
(pandingial-multiple? 9)
(pandingial-multiple? 192)
(pandingial-multiple? 987654321)
(pandingial-multiple? 1876543210)

(flatten (map range-9 (range 0 2)))

(apply max (map list-to-number (filter #(not (nil? %)) (map pandingial-multiple? (flatten (map range-9 (range 0 9)))))))



