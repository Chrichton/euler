(defn all-but-nth [coll n]
    "Returns a list of all items in coll except the nth item. (zero indexed)"
    (lazy-cat (take n coll) (drop (inc n) coll)))

(defn permutations [coll]
    "Returns a lazy list of all permutations of coll."
    (if (= 1 (count coll))
        [coll]
        (for [i (range (count coll)) p (permutations (all-but-nth coll i))]
            (lazy-cat [(nth coll i)] p))))

(defn list-to-number [l]
    (reduce + (map * l (reverse (take (count l) (iterate #(* 10 %) 1))))))

(list-to-number (last (take 1000000 (permutations (range 10)))))