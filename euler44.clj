(defn pentagon [n]
	(/ (* n (dec (* 3 n))) 2))

(pentagon 1)
(pentagon 2)
(pentagon 3)

(defn pentagons []
	(map pentagon (iterate inc 1)))

(take 4 (pentagons))

(defn pentagon? [n]
	(= n (first (drop-while #(< % n) (pentagons)))))

(pentagon? 22)
(pentagon? 21)

(for [j (range 1 5000) k (range 1 5000) :when (and (<= j k) (pentagon? (- (pentagon k) (pentagon j))) (pentagon? (+ (pentagon k) (pentagon j))))]
	(list k j (- (pentagon k) (pentagon j))))
