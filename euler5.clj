;(defn dividable? [n]
;  	(every? (fn [x] (zero? (mod n x))) (range 2 21))
;  )
;
;(time (first (filter (fn [n] (dividable? n)) (iterate inc (* 2 3 5 7 9 11 13 17 19)))))
;
;232792560

; "Elapsed time: 333060.684 msecs"

; Brain: 2 * 2 * 2 * 2 * 3 * 3 * 5 * 7 * 11 * 13 * 17 * 19 
; 1.Primfaktorzerlegung für alle Zahlen von 2 bis 20
; 2.Maximale Anzahl gleicher Primzahlen aus allen Zerlegungen 

(defn divCount [n factor]
	(loop [x n accu 0]
    	(if (zero? (rem x factor)) (recur (/ x factor) (inc accu)) accu)))
 
(defn primeFactors [primes]
	(fn [n]
		(loop [p primes accu '()]
        	(cond
            	(empty? p) (reverse accu)
                (not (zero? (rem n (first p)))) (recur (rest p) (cons 0 accu))
                :else (recur (rest p) (cons (divCount n (first p)) accu))))))
 
(def primeNumbers '(2 3 5 7 11 13 17 19))

((primeFactors primeNumbers) 6)
 
((primeFactors primeNumbers) 3528)
 
(map (primeFactors primeNumbers) (range 2 21))
(def exponents (reduce (fn [a b] (map max a b)) (map (primeFactors primeNumbers) (range 2 21))))
 
(apply * (map #(int(Math/pow %1 %2)) primeNumbers exponents))
