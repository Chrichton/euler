(defn multiple? [divider]
	(fn [n]
		(cond
			(<= n divider) true
  			:else (not (zero? (mod n divider)))
  		)))

(defn primes [n]
	(loop [primeslist (range 2 (inc n)) i 2]
		(cond
			(= i n) primeslist
  			:else (recur (filter (multiple? i) primeslist) (inc i))
  		)
  	)
)
  

(filter (multiple? 2) (range 2 10))
(filter (multiple? 3) (range 3 10))

(reverse (primes 47))

(some zero? '(1 2 3))

(filter (fn [n] (zero? (mod 600851475143 n))) (primes 600851475143))

(filter (multiple? 2) (range 2 600851))

(defn sieve [s]
    (cons (first s)
          (lazy-seq (sieve (filter (fn [n] (not (zero? (mod n (first s)))))
                                   (rest s))))))

(take-while (fn [prime] (< prime 20)) (sieve (iterate inc 2)))

(defn primesLazy [maxPrime]
	(take-while (fn [prime] (< prime maxPrime)) (sieve (iterate inc 2)))  
  )

(def p (primesLazy 600851475143))
(apply max (filter (fn [prime] (zero? (mod 600851475143 prime))) p))
