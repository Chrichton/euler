(defn factorial [n]
   {:pre [(>= n 0)] }
  	(apply * (range 1N (inc n))))

(defn selection-count [n r]
	(/ (factorial n) (* (factorial r) (factorial (- n r)))))

(selection-count 5 3)
(selection-count 23 10)

(count (for [n (range 23 101) r (range 23 101) 
	:when (and (<= r n) (> (selection-count n r) 1000000))]
		(list n r)))

