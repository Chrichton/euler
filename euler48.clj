(defn power [b x]
	(reduce * (repeat x b)))

(reduce + (map #(power % %) (range 1N 1001N)))