(defn two-times-squares []
	(map #(* 2 % %) (iterate inc 1)))

(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

(defn prime? [n]
   (if (< n 2) false
      (loop [x n acc 2]
          (cond 
            (= x acc) true
            (zero? (rem x acc)) false
            :else (recur x (inc acc))))))

(defn goldbach [n]
	(loop [ps (lazy-primes)]
		(let [p (first ps) delta (- n p)]
			#_(println n p)
			(cond
				(<= delta 0) false
				( = delta (first (drop-while #(< % delta) (two-times-squares)))) true
				:else (recur (drop 1 ps))))))
	

(some #(if (not (goldbach %)) %) (filter #(not (prime? %)) (iterate #(+ 2 %) 35)))
