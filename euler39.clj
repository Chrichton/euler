(defn tuples [perimeter]
	(for [a (range 1 perimeter) b (range 1 perimeter) 
		:when (and (<= a b) (zero? (+ (* perimeter perimeter) (* 2 a b) (* -2 a perimeter) (* -2 b perimeter))))]
			(list a b (+ perimeter (* -1 a) (* -1 b)))))

(tuples 120)

(defn max-tuple [t1 t2]
	(if (>= (first t1) (first t2)) t1 t2))

(second (reduce max-tuple (map #(list (count (tuples %)) %) (range 1 1001))))

