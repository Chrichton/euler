(defn palindrome? [n]
	(let [nString (str n)]
		(= nString (apply str (reverse nString)))))

(defn binary [n]
	(loop [x n accu '()]
		(if (zero? x) accu
			(recur (int (/ x 2)) (cons (rem x 2) accu)))))

(binary 585)

(defn palindrome-binary? [n]
	(let [b (binary n) rb (reverse b)]
		(= b rb)))
	
(palindrome-binary? 585)
(palindrome-binary? 584)

(apply + (filter #(and (palindrome? %) (palindrome-binary? %)) (range 1 1E6 2)))
