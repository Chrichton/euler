(defn triangle [n]
	(/ (* n (inc n)) 2))

(defn pentagonal [n]
	(/ (* n (dec (* 3 n))) 2))

(defn hexagonal [n]
	(* n (dec (* 2 n))))

(triangle 285) 
(pentagonal 165) 
(hexagonal 143)

(defn triangles []
	(map triangle (iterate inc 1)))

(defn pentagonals []
	(map pentagonal (iterate inc 1)))

(defn hexagonals []
	(map hexagonal (iterate inc 1)))

(defn find-all []
	(loop [ts (drop-while #(< % 40756) (triangles)) ps (drop-while #(< % 40756) (pentagonals)) hs (drop-while #(< % 40756) (hexagonals))]
		(let [t (first ts) p (first ps) h (first hs)]
			(cond
				(= t p h) t
				(< t p) (recur (drop-while #(< % p) ts) ps hs)
				(< t h) (recur (drop-while #(< % h) ts) ps hs)
				(< h p) (recur ts ps (drop-while #(< % p) hs))
				:else (recur ts (drop-while #(< % h) ps) hs)))))

(find-all)
