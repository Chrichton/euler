
(defn nth-prime [n]
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (first (drop (dec n) (cons 2 (lazy-seq (next-prime 3 [])))))))

(nth-prime 10001)