(defn palindrome? [n]
	(let [nString (str n)]
		(= nString (apply str (reverse nString)))))

(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn list-to-number [l]
    (reduce + (map * l (reverse (take (count l) (iterate #(* 10 %) 1N))))))

(defn reverse-number [n]
	(list-to-number (reverse (digits n))))

(defn next-number [n]
	(+ n (reverse-number n)))

(next-number 47)

(defn lychrel? [n]
	(loop [n1 (next-number n) cnt 1]
		(cond
			(>= cnt 50) true
			(palindrome? n1) false
			:else (recur (next-number n1) (inc cnt)))))

(lychrel? 47)
(lychrel? 349)
(lychrel? 10677)
(lychrel? 196)
(lychrel? 4994)
(lychrel? 9999)

(count (filter lychrel? (range 1 10000)))
