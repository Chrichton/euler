(defn prime? [n]
   (if (< n 2) false
      (loop [x n acc 2]
          (cond 
            (= x acc) true
            (zero? (rem x acc)) false
            :else (recur x (inc acc))))))

(defn quadratic-form [a b]
	(fn [n] (+ (* n n) (* a n) b)))

((quadratic-form 1 41) 2)

(defn quadratic-form-seq [a b]
	(map (quadratic-form a b) (iterate inc 0)))

(defn no-of-primes [a b]
	(count (take-while prime? (quadratic-form-seq a b))))

(no-of-primes 1 41)
(no-of-primes -79 1601)

(defn max-prime-seq [p1 p2]
  (if (>= (first p1) (first p2)) p1 p2))  

(last
  (reduce max-prime-seq 
    (for [a (range -999 1000) b (range -999 1000)]
	     (list (no-of-primes a b) (* a b)))))
