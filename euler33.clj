(denominator (apply * (for [numerator (range 1 99) denominator (range 1 99) :when 
	(and 
		(< numerator denominator) 
		(not (zero? (mod numerator 10))) 
		(not (zero? (mod denominator 10)))
		(= (mod numerator 10) (int (/ denominator 10)))
		(= (/ numerator denominator) (/ (int (/ numerator 10)) (mod denominator 10))))]
	(/ numerator denominator))))
