(defn cyc-length [[l1 l2 l3 & r]]
	(if (= l1 l2 l3) (count l1) 0))
		
(cyc-length '((7 7 7 8) (7 7 7 7))) ; 0
(cyc-length '((7 7 7 7) (7 7 7 7))) ; 4
(cyc-length '((7 7 7 1 2) (7 7 7 1 2))) ; 5

#_(defn cycle-length[l]
	(let [cnt (count l)]
		(if (<= cnt 3) 0 (cyc-length (partition (int (/ cnt 2)) l)))))

(defn cycle-length [l]
	(loop [local-l l tries 0]
		(cond (>= tries 10) 0
		(<= (count local-l) 5) 0
		:else (let [len (cyc-length (partition (int (/ (count local-l) 3)) local-l))]
					(if (zero? len) (recur (take (dec (count local-l)) local-l) (inc tries)) len)))))

(cycle-length '(7 7 7 7 7 8)) ; 0
(cycle-length '(3 2 1 3 2 1 3 2 1)) ; 3

(defn reciprocal-cycle [n]
	(loop [divident 10 accu '() last-divident nil]
		;(println "div" divident "last-div" last-divident "accu" accu)
		(cond
			(zero? (rem divident n)) 0
			(= divident last-divident) 1 										; Periode
			(not (zero? (cycle-length accu))) (cycle-length accu)               ; echter Zyklus
			(< divident n) (recur (* 10 divident) (cons 0 accu) divident)
			:else (recur (* 10 (rem divident n)) (cons (int (/ divident n)) accu) divident))))

(reciprocal-cycle  2) ; 0 (0.5)
(reciprocal-cycle  3) ; 1 (0.3333)
(reciprocal-cycle  4) ; 0 (0.25)
(reciprocal-cycle  5) ; 0 (0.2)
(reciprocal-cycle  6) ; 1 (0.1666)
(reciprocal-cycle  7) ; 6 (0.142857 142857)
(reciprocal-cycle 11) ; 2 (0.09 09)
(reciprocal-cycle 13) ; 6 (0.076923 076923)
(reciprocal-cycle 14) ; 6 (0.0 714285 714285)
(reciprocal-cycle 17) ;16 (0.0588235294117647 0588235294117647)
(reciprocal-cycle 28) ; 6 (0.03 571428 571428)
(reciprocal-cycle 991) ; 495 

(apply max (map reciprocal-cycle (range 2 1000)))
