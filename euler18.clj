(def input 
"75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23")

(defn values [s]
	 (map read-string (map #(clojure.string/replace % #"0([0-9])" "$1") 
	 	(clojure.string/split s #"\s"))))
(defn to-lines [s] 
	(map #(clojure.string/replace % #"0([0-9])" "$1") (clojure.string/split-lines s)))

(defn to-triangle [s]
	(map #(map read-string (seq (clojure.string/split % #"\s"))) (to-lines s)))

#_(defn distance [v1 x1 v2 x2]
	(- (.indexOf v2 x2) (.indexOf v1 x1)))

(defn compress [l]
  	(flatten (cons (first l) (map #(apply max %) (partition-all 2 (drop 1 l))))))

#_(defn cross-sum [l1 l2]
	(compress
	  	(for [x1 l1 x2 l2 :when (or (zero? (distance l1 x1 l2 x2)) (= 1 (distance l1 x1 l2 x2)))]
	  		(+ x1 x2))))

(defn pair-sums [l]
	(reverse
  	(loop [l1 l accu '()]
  		(if (< (count l1) 2) accu
  			(recur (drop 1 l1) (cons (apply + (take 2 l1)) accu))))))

(defn cross-sum [l1 l2]
	(compress ( pair-sums (cons (first l2) (interleave l1 (drop 1 l2))))))

(compress '(187 217 186 221))

(cross-sum '(75) '(95 64))
(cross-sum '(170 139) '(17 47 82))
(cross-sum '(187 217 221) '(18 35 87 10))
(cross-sum '(205 252 308 231) ('20 04 82 47 65))
(count (cross-sum '(1 2 3 4 5 6 7 8) '(11 12 13 14 15 16 17 18 19)))
(count (cross-sum '(431 397 494 566 544 488 491 489) '(41 41 26 56 83 40 80 70 33)))

(reduce cross-sum '((3) (7 4) (2 4 6) (8 5 9 3)))
(apply max (reduce cross-sum '((3) (7 4) (2 4 6) (8 5 9 3))))

(apply max (reduce cross-sum (to-triangle input)))
