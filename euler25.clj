(defn digits [n]
  (map #(Integer/parseInt (str %))  (seq (str n))))

(defn fib-lazy []
	(map first (iterate (fn [[n1 n2]] (list n2 (+ n1 n2))) (list 1N 1N))))

;(take 12 (map #(list (digits %1) %2) (fib-lazy) (iterate inc 1)))

;(last (first (drop-while #(< (count (first %1)) 3) (map (fn [fib idx] (list (digits fib) idx)) (fib-lazy) (iterate inc 1)))))

(last (first (drop-while #(< (count (first %1)) 1000) (map (fn [fib idx] (list (digits fib) idx)) (fib-lazy) (iterate inc 1)))))
