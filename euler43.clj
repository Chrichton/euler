(defn all-but-nth [coll n]
    "Returns a list of all items in coll except the nth item. (zero indexed)"
    (lazy-cat (take n coll) (drop (inc n) coll)))

(defn permutations [coll]
    "Returns a lazy list of all permutations of coll."
    (if (= 1 (count coll))
        [coll]
        (for [i (range (count coll)) p (permutations (all-but-nth coll i))]
            (lazy-cat [(nth coll i)] p))))

(defn list-to-number [l]
    (reduce + (map * l (reverse (take (count l) (iterate #(* 10 %) 1))))))

(defn sub-divisible? [pandigital]
	(and
		(not (odd? (first (drop 3 pandigital))))
		(zero? (rem (list-to-number (take 3 (drop 2 pandigital))) 3))
		(zero? (rem (first (drop 5 pandigital)) 5))
		(zero? (rem (list-to-number (take 3 (drop 4 pandigital))) 7))
		(zero? (rem (list-to-number (take 3 (drop 5 pandigital))) 11))
		(zero? (rem (list-to-number (take 3 (drop 6 pandigital))) 13))
		(zero? (rem (list-to-number (take 3 (drop 7 pandigital))) 17))))

(sub-divisible? '(1 4 0 6 3 5 7 2 8 9))
(sub-divisible? '(4 0 6 3 5 7 2 8 9 1))

(apply + (for [pandigital (permutations '(1 2 3 4 5 6 7 8 9 0)) 
	:when (and (not= 0 (first pandigital)) (sub-divisible? pandigital))]
		(list-to-number pandigital)))
