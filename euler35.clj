(defn log10 [n] (/ (Math/log n) (Math/log 10)))

(defn pow10 [e] (int(Math/pow 10 e)))

(defn gen-cycle [n]
  (+ (int (/ n 10)) (* (rem n 10) (pow10 (int (log10 n))))))

(gen-cycle 197)
(gen-cycle 190)

(defn gen-cycles [n]
  (if (< n 10) '()
    (let [number-of-digits (int (log10 n))]
      (loop [accu (list (gen-cycle n))]
        (if (= (count accu) number-of-digits) (distinct accu)
          (recur (cons (gen-cycle (first accu)) accu)))))))
    
(gen-cycles 197)  ; (971 719)
(gen-cycles 1)    ; ()
(gen-cycles 11)   ; (11)
(gen-cycles 1234) ; (2341 3412 4123)

(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

(defn prime? [n]
   (if (< n 2) false
      (loop [x n acc 2]
          (cond 
            (= x acc) true
            (zero? (rem x acc)) false
            :else (recur x (inc acc))))))

(defn all-cycles-are-primes? [n]
   (every? prime? (gen-cycles n)))  

(all-cycles-are-primes? 2)

(count (filter all-cycles-are-primes? (take-while #(< % 100) (lazy-primes))))
(count (filter all-cycles-are-primes? (take-while #(< % 1e6) (lazy-primes))))