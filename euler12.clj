(defn triangels []
	(reductions + (iterate inc 1)))

(defn number-of-divisors [number]
	(let [sqrt (int (Math/sqrt number))]
		(loop [i 1 nod 0]
			(cond
				(> i sqrt) (if (= (* sqrt sqrt) number) (dec nod) nod)
				(zero? (mod number i)) (recur (inc i) (+ nod 2))
				:else (recur (inc i) nod)))))

(first (drop-while #(< (number-of-divisors %) 500) (triangels)))
