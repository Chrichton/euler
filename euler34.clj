(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn fact [n]
	(apply * (range 1 (inc n))))

(fact 6)

(defn sum-of-factorials? [n]
	(= n (apply + (map fact (digits n)))))

(sum-of-factorials? 145)
(sum-of-factorials? 146)

(filter sum-of-factorials? (range 10 1000000))