(reverse "11")
(str (reverse "11"))
(apply str (reverse "11"))

(defn palindrome? [n]
	(let [nString (str n)]
		(= nString (apply str (reverse nString)))))

(palindrome? 1)
(palindrome? 11)
(palindrome? 101)
(palindrome? 12)

(def palindromes 
	(filter palindrome? (range (* 999 999) (* 100 100) -1))
  )

(take 10 palindromes)

(def threeDigitNumbers
	(range 999 99 -1))

(defn productOfThreeDigitsFactor? [n]
	(fn [f]
		(some (fn [x] (= (/ n x) f)) threeDigitNumbers))
  )

((productOfThreeDigitsFactor? 10000) 100)
((productOfThreeDigitsFactor?  9900) 100)

(defn productOfTwoThreeDigitsFactors? [n]
	(some (productOfThreeDigitsFactor? n) threeDigitNumbers)
  )

(productOfTwoThreeDigitsFactors? 10000)
(productOfTwoThreeDigitsFactors?  9999)

(first (filter (fn [x] (productOfTwoThreeDigitsFactors? x)) palindromes))
