(defn triangle-numbers []
	(map #(* (/ 1 2) % (inc %)) (iterate inc 1)))

(take 10 (triangle-numbers))

(defn char-value 
   	([] 0)
    ([c] (if (= c \") 0 (- (int c) 64)))
    ([c1 c2] (+ (char-value c1) (char-value c2))))
  
 (defn string-value [s]
    (reduce + (map char-value (seq s))))

(string-value "SKY")

(defn triangle? [n]
	(= n (first (drop-while #(< % n) (triangle-numbers)))))

(triangle? 55)
(triangle? 56)

(count (filter triangle? (map string-value (map read-string (clojure.string/split (slurp "words.txt") #",")))))
