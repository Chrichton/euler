(defn coins []
	'(200 100 50 20 10 5 2 1))

(defn coin-sums [coins sum]
		(cond
			(empty? coins) 0
			(= sum 200) 1
			(> sum 200) 0
			:else (+ (coin-sums coins (+ sum (first coins))) (coin-sums (rest coins) sum))))

(coin-sums (coins) 0)
