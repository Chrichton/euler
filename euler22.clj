(def input (slurp "C:\\Users\\heiko\\clojure\\euler\\names.txt"))

(defn char-value 
	([] 0)
	([c] (if (= c \") 0 (- (int c) 64)))
	([c1 c2] (+ (char-value c1) (char-value c2))))

(defn string-value [s]
	(reduce + (map char-value (seq s))))

(reduce + (map char-value (seq "AAA")))
(string-value "AAA")
(string-value "AARON"); 49

;(reduce char-value (seq "AAA"))

(reduce + (map #(* %1 %2) (map string-value (sort (clojure.string/split input #","))) (iterate inc 1)))

