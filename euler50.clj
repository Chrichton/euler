(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs]
            (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
                (recur (+ x 2) xs)
                (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))

(defn prime? [n]
   (if (< n 2) false
        (loop [x n acc 2]
            (cond
                (= x acc) true
                (zero? (rem x acc)) false
                :else (recur x (inc acc))))))
 
(defn sum-count [[d t]]
    (let [[prime-sum :as pc]
        (last (take t (map #(list %1 %2)
            (reductions + (drop d (lazy-primes)))
            (reductions + (repeat 1)))))]
 
        (if (prime? prime-sum) pc '(0 0))))

(defn max-sum-count [[sum1 cnt1 :as a]  [sum2 cnt2 :as b] ]
    ( if( > cnt1 cnt2) a b))

(defn list-of-ranges [n]
    (let [max-summands (count (take-while #(< % n) (reductions + (lazy-primes))))]
        (for [ d (range (dec max-summands)) t (range 2 (inc max-summands)) :when (<= d (- max-summands t))]
            (list d t))))

(defn max-sum [n]
    (loop [[[_ length :as calc-range] & r] (list-of-ranges n) [prime-sum summand-count :as acc] '(0 0)]
        (cond
            (nil? calc-range) prime-sum                                     ;Terminate;    
            (>= summand-count length) (recur r acc)                         ;Skip
            :else (recur r (max-sum-count acc (sum-count calc-range))))))   ;Calculate
 
(max-sum 1e6)