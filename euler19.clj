(defn leap-year? [year]
	(and (zero? (mod year 4)) (or (not (zero? (mod year 100))) (zero? (mod year 400)))))

(defn days-in-month [year]
	(fn [month]
		{:pre [(and (> month 0) (< month 13))]}
		(cond
			(= month 2) (if (leap-year? year) 29 28)
			(and (<= month 7) (odd? month)) 31
			(and (>= month 8) (not (odd? month))) 31
			:else 30)))


(defn days-in-year [year]
	(reduce + (map (days-in-month year) (range 1 13))))

(defn days-since-1900 [year month]
	{:pre [(>= year 1900)]}
	(+ 	(reduce + (map days-in-year (range 1900 year)))
		(reduce + (map (days-in-month year) (range 1 month)))))

(days-in-year 1200) ;366
(days-in-year 1204) ;366
(days-in-year 3000) ;365
(days-in-year 2001) ;365

(days-since-1900 1901 1) ; 365
(days-since-1900 1901 2) ; 396

(defn sunday? [year month]
	(= 6 (mod (days-since-1900 year month) 7)))

(sunday? 2012 7) ; true
(sunday? 2012 6) ; false

(count (filter #(sunday? (first %) (last %)) (for [year (range 1901 2001) month (range 1 13)]
	(list year month))))
