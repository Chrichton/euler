(defn fib-lazy []
    (map first (iterate (fn [[n1 n2]] (list n2 (+ n1 n2))) (list 1N 1N))))

(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn head-tail-pandangial? [s]
	(and (= 10 (count (distinct (take 10 s)))) (= 10 (count (distinct (take 10 (reverse s)))))))

(head-tail-pandangial? (digits 12345678901234567890))
(head-tail-pandangial? (digits 12345678901234567891))

(first (drop-while #(or (<= (second % 2749) (not (head-tail-pandangial? (digits (first %))))) (map #(list %1 %2) (fib-lazy) (iterate inc 1)))))
