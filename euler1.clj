(defn multiplesOf [n below]
  (range n below n))

(multiplesOf 3 10)

(apply + (into (set (multiplesOf 5 10)) (set (multiplesOf 3 10))))

(apply + (into (set (multiplesOf 5 1000)) (set (multiplesOf 3 1000))))
