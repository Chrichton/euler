(defn abundant [n]
	(> (apply + (filter #(zero? (rem n %)) (range 1 n))) n))

(abundant 12) ;true
(abundant 21) ;false

(defn abundants [up-to]
	(filter abundant (range 12 (inc up-to))))

(def my-abundants (abundants 28112))

(time (apply + (clojure.set/difference (set (range 1 28124)) (set (for [row my-abundants col my-abundants :when (>= row col)]
	(+ row col))))))
