(defn lazy-primes []
    (let [next-prime
        (fn next-prime [x xs] 
          (if (some #(zero? (rem x %)) (take-while #(<= (* % %) x) xs))
              (recur (+ x 2) xs)
              (cons x (lazy-seq (next-prime (+ x 2) (conj xs x))))))]
            (cons 2 (lazy-seq (next-prime 3 [])))))
 
(defn prime-factor-count [primes n]
		(loop [p primes accu 0]
        	(cond
            	(> (first p) n) accu
              (not (zero? (rem n (first p)))) (recur (rest p) accu)
              :else (recur (rest p) (inc accu)))))

(prime-factor-count (lazy-primes) 644)
(prime-factor-count (lazy-primes) 65)
(prime-factor-count (lazy-primes) 5)
(prime-factor-count (lazy-primes) 6)

(defn consecutive-primes? [quad]
    (every? #(= 4 %) quad))

(consecutive-primes? '(4 4 4 4))
(consecutive-primes? '(4 4 4 3))

(defn next-quad [primes] 
    (fn [[quad n]]
        (list (cons (prime-factor-count primes (inc n)) (take 3 quad)) (inc n))))

((next-quad (lazy-primes)) '(() 1))
((next-quad (lazy-primes)) '((1) 2))
((next-quad (lazy-primes)) '((1 1) 3))
((next-quad (lazy-primes)) '((1 1 1) 4))
((next-quad (lazy-primes)) '((1 1 1 1) 5))
((next-quad (lazy-primes)) '((2 1 1 1) 6))

(take 6 (iterate (next-quad (lazy-primes)) '((1) 2)))

(- (second (first (drop-while #(not (consecutive-primes? (first %))) (iterate (next-quad (lazy-primes)) '((1) 2))))) 3)
