(defn next-collatz [n]
	(if (odd? n) (inc (* 3 n)) (/ n 2)))

(defn length-collatz [n]
	(+ 2 (count (take-while #(not= % 1 ) (iterate next-collatz (next-collatz n))))))

(length-collatz 1)
(length-collatz 13)

(defn max-collatz [[length1 :as c1] [lenght2 :as c2]]
	(if (> length1 lenght2) c1 c2))

(last (reduce max-collatz (map #(list %1 %2) (map length-collatz (range 1 1e6)) (range 1 1e6))))
