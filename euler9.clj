(defn gen-next-sum [actual-sum]
  	(let [a (inc (first actual-sum)) b (dec (second actual-sum)) c (last actual-sum)]
  		(if (< a b) (list a b c)
  				(let [c1 (dec c)]
  					(if (>= c1 500) (list 1 (- 999 c1) c1) 
                (if (>= c1 335) 
                    (let [b1 (dec c1) a1 (- 1000 b1 c1)]
                        (list a1 b1 c1))
                    '()))))))

(gen-next-sum '(1     2 997)) ; '(1 3 996)
(gen-next-sum '(1     3 996)) ; '(1 4 995)
(gen-next-sum '(1     4 995)) ; '(2 3 995)
(gen-next-sum '(2     3 995)) ; '(1 5 994)
(gen-next-sum '(1     5 994)) ; '(2 4 994)

(gen-next-sum '(247 253 500)) ; '(248 250 500)
(gen-next-sum '(248 252 500)) ; '(249 251 500)
(gen-next-sum '(249 251 500)) ; '(  3 498 499)


(defn sums-of-1000 []
	(loop [accu '((1 2 997))]
		(let [next-sum (gen-next-sum (first accu))]
			(if (empty? next-sum) accu
				(recur (cons next-sum accu))))))

(defn square [n]
  (* n n))

(square 10)

(defn triplet? [actual-sum]
  (= (+ (square (first actual-sum)) (square (second actual-sum))) (square (last actual-sum))))

(triplet? '(3 4 5)) ;  true
(triplet? '(3 4 6)) ; false

(first (sums-of-1000)) ; 
(count (sums-of-1000)) ; 82834

(filter triplet? (sums-of-1000))
(apply * (first (filter triplet? (sums-of-1000))))
