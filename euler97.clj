(defn super-mult [x y]
	(let [product (* x y)]
		(if (> product 9999999999)
			(BigInteger. (subs (str product) 1))
			product)))

(defn power [b x]
	(reduce super-mult (repeat x b)))

(inc (* 28433N (power 2N 7830457N)))
