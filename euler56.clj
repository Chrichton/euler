(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn power [b x]
    (apply * (repeat x b)))

(apply max (for [a (range 1N 100N) b (range 1N 100N)] 
	(apply + (digits (power a b)))))
