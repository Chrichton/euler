(defn digits [n]
	(map #(Integer/parseInt (str %))  (seq (str n))))

(defn next-chain-number [n]
	(apply + (map #(* % %) (digits n))))

(next-chain-number 44)
(next-chain-number 32)
(next-chain-number 13)
(next-chain-number 10)

(defn chain-result [n]
	(first (drop-while #(and (not= % 1) (not= % 89)) (iterate next-chain-number n))))

(chain-result 44)
(chain-result 145)

(count (filter #(= 89 %) (map chain-result (range 1 1E7))))
