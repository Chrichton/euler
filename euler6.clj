(defn square [n]
  	(* n n)
  )

(defn sumOfSquares [n]
  	(apply + (take n (map square (iterate inc 1))))
  )

(defn squareOfSum [n]
	(square (* (/ n 2) (+ n 1))) ; Gaussian formula
  )

(- (squareOfSum 100) (sumOfSquares 100))
