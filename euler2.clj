(defn fib [n]
		(cond
			(= n 0) 1
			(= n 1) 2
			:else (+ (fib (- n 2)) (fib (- n 1)))

		)
	)

(defn fibmap [tupel]
	(vector (second tupel) (+ (first tupel) (second tupel)))
	)

(take 5 (iterate fibmap '[1 2]))

(apply + (filter even? (take-while (fn [n] (<= n 4e6)) (map first (iterate fibmap '[1 2])))))
