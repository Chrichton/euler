(defn sorted-digits [n]
	(sort (map #(Integer/parseInt (str %))  (seq (str n)))))

(some #(if (= (sorted-digits (* 2 %)) 
	(sorted-digits (* 3 %)) 
	(sorted-digits (* 4 %)) 
	(sorted-digits (* 5 %)) 
	(sorted-digits (* 6 %))) %) (iterate inc 1))
